/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author Yumat
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String img;

    public Product(int id, String name, double price, String img) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImg() {
        return img;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", img=" + img + '}';
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"Espresso",50,"Espresso Machiato.png"));
        list.add(new Product(2,"Espresso",40,"espresso.png"));
        list.add(new Product(3,"Hot Chocolate",45,"horchoc.png"));
        list.add(new Product(4,"Hot Thai Milk Tea",40,"hotthaimiletea.png"));
        list.add(new Product(5,"Iced Caramel Macchiato",45,"Iced Caramel Macchiato.png"));
        list.add(new Product(6,"Italian Soda",40,"Italian Soda.png"));
        return list;
    }
}
